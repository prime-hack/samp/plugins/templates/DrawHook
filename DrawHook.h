#pragma once

#include <cstdint>
#if __has_include( <SRSignal/SRSignal.hpp>)
#	include <SRSignal/SRSignal.hpp>
#endif
#if __has_include( <llmo/SRHookFast.h>)
#	include <llmo/SRHookFast.h>
#endif

#ifdef _MSVC_VER
struct IDirect3DDevice9;
#else
class IDirect3DDevice9;
#endif

class DrawHook {
public:
	// types
	struct arg {
		using color_t = const uint32_t &;
		using prect_t = float *;
		using state_t = uint32_t;
		using stage_t = uint32_t;
		using type_t = uint32_t;
		using value_t = uint32_t;
		using count_t = uint32_t;
		using pverticies_t = void *;
		using posx_t = int32_t;
		using posy_t = int32_t;
	};

private:
#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <SRSignal/SRSignal.hpp>)
	SRHook::Fast::Hook hook_draw{ 0x53EBF5, 5 };
#	endif
	SRHook::Fast::Hook hook_lost{ 0x7F8152, 5 };
	SRHook::Fast::Hook hook_reset{ 0x4CC979, 5 };

#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	std::deque<class CD3DFont *> m_fontList;
	std::deque<class CD3DRender *> m_renderList;
	std::deque<class SRTexture *> m_textureList;
#	endif
#endif

	typedef void( __cdecl *rw_renderStateSet_t )( arg::state_t state, arg::value_t value );
	typedef void( __cdecl *rw_renderStateGet_t )( arg::state_t state, arg::value_t &lValue );
	typedef void( __cdecl *rw_setRasterStage_t )( struct RwRaster *raster, arg::stage_t stage );
	typedef struct RwRaster *( __cdecl *rw_getRasterStage_t )( arg::stage_t stage );
	typedef void( __cdecl *rw_setTextureStageState_t )( arg::stage_t stage, arg::type_t type, arg::value_t value );
	typedef uint32_t( __cdecl *rw_getTextureStageState_t )( arg::stage_t stage, arg::type_t type, arg::value_t &lValue );
	typedef void( __cdecl *rw_render2DPrim_t )( arg::type_t type, arg::pverticies_t verts, arg::count_t count );
	typedef void( __cdecl *rw_pushContext_t )( struct RwRaster *raster );
	typedef void( __cdecl *rw_popContext_t )();
	typedef void( __cdecl *rw_fastRender_t )( struct RwRaster *raster, arg::posx_t x, arg::posy_t y );
	typedef void( __cdecl *rw_DrawRect_t )( arg::prect_t rect, arg::color_t color );
	typedef void(
		__cdecl *rw_DrawGrad_t )( arg::prect_t rect, arg::color_t cl_lu, arg::color_t cl_ru, arg::color_t cl_lb, arg::color_t cl_rb );

public:
	DrawHook();
	~DrawHook();

	// base events
#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <SRSignal/SRSignal.hpp>)
	SRSignal<> onDraw;
	SRSignal<> onPreReset;
	SRSignal<> onPostReset;
#	endif
#endif


	// d3d9 methods
	static IDirect3DDevice9 *d3d9_device();

#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	class CD3DFont *
		d3d9_createFont( std::string_view fontName = "", int fontHeight = 15, DWORD dwCreateFlags = 4, DWORD dwCharSet = DEFAULT_CHARSET );
	bool d3d9_releaseFont( class CD3DFont *pFont );

	class CD3DRender *d3d9_createRender( int numVertices = 128 );
	bool d3d9_releaseRender( class CD3DRender *pRender );

	class SRTexture *d3d9_createTexture( int width, int height );
	bool d3d9_releaseTexture( class SRTexture *pTexture );
#	endif
#endif


	// rw methods
	static rw_renderStateSet_t &rw_renderStateSet;
	static rw_renderStateGet_t &rw_renderStateGet;

	static rw_setRasterStage_t &rw_setRasterStage;
	static rw_getRasterStage_t &rw_getRasterStage;

	static rw_setTextureStageState_t &rw_setTextureStageState;
	static rw_getTextureStageState_t &rw_getTextureStageState;

	static rw_render2DPrim_t &rw_render2DPrim;

	static rw_pushContext_t &rw_pushContext;
	static rw_popContext_t &rw_popContext;

	static rw_fastRender_t &rw_fastRender;

	static rw_DrawRect_t &rw_DrawRect;
	static rw_DrawGrad_t &rw_DrawGrad;

#if __has_include( <llmo/callfunc.hpp>)
	static struct RwRaster *rw_loadPNG( const char *filename );
	static struct RwRaster *rw_loadPNG( uint8_t *addr, uint32_t size );
#endif

protected:
#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <SRSignal/SRSignal.hpp>)
	void draw();
#	endif
	void lost();
	void reset();
#endif

	static uint32_t &rwInstance;

private:
#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	template<class C> bool d3d9_release( std::deque<C *> deque, C *item ) {
		auto it = std::find( deque.begin(), deque.end(), item );
		if ( it != deque.end() ) {
			deque.erase( it );
			delete item;
			return true;
		}
		return false;
	}
#	endif
#endif
};
