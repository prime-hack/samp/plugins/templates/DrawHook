#include "DrawHook.h"
#if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
#	include <render/d3drender.h>
#	include <render/texture.h>
#endif
#if __has_include( <llmo/callfunc.hpp>)
#	include <llmo/callfunc.hpp>
#endif

namespace { // gta_sa 1.0us (and compact)
	constexpr uint32_t _setRasterStage = 0x7FDCD0;
	constexpr uint32_t _getRasterStage = 0x7FDE50;
	constexpr uint32_t _setTextureStageState = 0x7FC340;
	constexpr uint32_t _getTextureStageState = 0x7FC3A0;
	constexpr uint32_t _pushContext = 0x7FB060;
	constexpr uint32_t _popContext = 0x7FB110;
	constexpr uint32_t _fastRender = 0x7FAF50;
	constexpr uint32_t _DrawRect = 0x727B60;
	constexpr uint32_t _DrawGrad = 0x727C10;
} // namespace

uint32_t &DrawHook::rwInstance = *reinterpret_cast<uint32_t *>( 0xC97B24 );
DrawHook::rw_renderStateSet_t &DrawHook::rw_renderStateSet = *(DrawHook::rw_renderStateSet_t *)( rwInstance + 0x20 );
DrawHook::rw_renderStateGet_t &DrawHook::rw_renderStateGet = *(DrawHook::rw_renderStateGet_t *)( rwInstance + 0x24 );
DrawHook::rw_setRasterStage_t &DrawHook::rw_setRasterStage = *(DrawHook::rw_setRasterStage_t *)&_setRasterStage;
DrawHook::rw_getRasterStage_t &DrawHook::rw_getRasterStage = *(DrawHook::rw_getRasterStage_t *)&_getRasterStage;
DrawHook::rw_setTextureStageState_t &DrawHook::rw_setTextureStageState = *(DrawHook::rw_setTextureStageState_t *)&_setTextureStageState;
DrawHook::rw_getTextureStageState_t &DrawHook::rw_getTextureStageState = *(DrawHook::rw_getTextureStageState_t *)&_getTextureStageState;
DrawHook::rw_render2DPrim_t &DrawHook::rw_render2DPrim = *(DrawHook::rw_render2DPrim_t *)( rwInstance + 0x30 );
DrawHook::rw_pushContext_t &DrawHook::rw_pushContext = *(DrawHook::rw_pushContext_t *)&_pushContext;
DrawHook::rw_popContext_t &DrawHook::rw_popContext = *(DrawHook::rw_popContext_t *)&_popContext;
DrawHook::rw_fastRender_t &DrawHook::rw_fastRender = *(DrawHook::rw_fastRender_t *)&_fastRender;
DrawHook::rw_DrawRect_t &DrawHook::rw_DrawRect = *(DrawHook::rw_DrawRect_t *)&_DrawRect;
DrawHook::rw_DrawGrad_t &DrawHook::rw_DrawGrad = *(DrawHook::rw_DrawGrad_t *)&_DrawGrad;

DrawHook::DrawHook() {
#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <SRSignal/SRSignal.hpp>)
	hook_draw.onBefore += std::tuple{ this, &DrawHook::draw };
	hook_draw.install();
#	endif
	hook_lost.onBefore += std::tuple{ this, &DrawHook::lost };
	hook_lost.install();
	hook_reset.onBefore += std::tuple{ this, &DrawHook::reset };
	hook_reset.install();
#endif
}

DrawHook::~DrawHook() {
#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	for ( auto &&pFont : m_fontList ) delete pFont;
	for ( auto &&pRender : m_renderList ) delete pRender;
	for ( auto &&pTexture : m_textureList ) delete pTexture;
#	endif
#endif
}

IDirect3DDevice9 *DrawHook::d3d9_device() {
	return *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 );
}

#if __has_include( <llmo/SRHookFast.h>)
#if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
CD3DFont *DrawHook::d3d9_createFont( std::string_view fontName, int fontHeight, DWORD dwCreateFlags, DWORD dwCharSet ) {
	auto pFont = new CD3DFont( fontName, fontHeight, dwCreateFlags, dwCharSet );
	pFont->Initialize( d3d9_device() );
	m_fontList.push_back( pFont );
	return pFont;
}

bool DrawHook::d3d9_releaseFont( CD3DFont *pFont ) {
	return d3d9_release( m_fontList, pFont );
}

CD3DRender *DrawHook::d3d9_createRender( int numVertices ) {
	auto pRender = new CD3DRender( numVertices );
	pRender->Initialize( d3d9_device() );
	m_renderList.push_back( pRender );
	return pRender;
}

bool DrawHook::d3d9_releaseRender( CD3DRender *pRender ) {
	return d3d9_release( m_renderList, pRender );
}

SRTexture *DrawHook::d3d9_createTexture( int width, int height ) {
	auto pTexture = new SRTexture( width, height );
	pTexture->Initialize( d3d9_device() );
	m_textureList.push_back( pTexture );
	return pTexture;
}

bool DrawHook::d3d9_releaseTexture( SRTexture *pTexture ) {
	return d3d9_release( m_textureList, pTexture );
}
#endif
#endif

#if __has_include( <llmo/callfunc.hpp>)
RwRaster *DrawHook::rw_loadPNG( const char *filename ) {
	if ( !filename || !*filename ) return nullptr;
	auto image = CallFunc::ccall<class RwImage *>( 0x7CF9B0, filename );
	int width, height, depth, format;
	CallFunc::ccall( 0x8042C0, image, 4, &width, &height, &depth, &format );
	auto raster = CallFunc::ccall<RwRaster *>( 0x7FB230, width, height, depth, format );
	CallFunc::ccall( 0x804290, raster, image );
	CallFunc::ccall( 0x802740, image );
	return raster;
}

RwRaster *DrawHook::rw_loadPNG( uint8_t *addr, uint32_t size ) {
	if ( !addr || !size ) return nullptr;
	struct {
		uint8_t *memptr;
		uint32_t memSize;
	} data{ addr, size };
	memsafe::write( 0x7CF9CA, '\x03' );
	auto raster = rw_loadPNG( (const char *)&data );
	memsafe::write( 0x7CF9CA, '\x02' );
	return raster;
}
#endif

#if __has_include( <llmo/SRHookFast.h>)
#	if __has_include( <SRSignal/SRSignal.hpp>)
void DrawHook::draw() {
	if ( onDraw.empty() ) return;

	IDirect3DStateBlock9 *m_pD3Dstate = nullptr;
	if ( FAILED( d3d9_device()->CreateStateBlock( D3DSBT_ALL, &m_pD3Dstate ) ) ) m_pD3Dstate = nullptr;

	onDraw();

	if ( m_pD3Dstate ) {
		m_pD3Dstate->Apply();
		m_pD3Dstate->Release();
	}
}
#	endif

void DrawHook::lost() {
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	for ( auto &&pFont : m_fontList ) pFont->Invalidate();
	for ( auto &&pRender : m_renderList ) pRender->Invalidate();
	for ( auto &&pTexture : m_textureList ) pTexture->Invalidate();
#	endif

#	if __has_include( <SRSignal/SRSignal.hpp>)
	onPreReset();
#	endif
}

void DrawHook::reset() {
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	for ( auto &&pFont : m_fontList ) pFont->Initialize( d3d9_device() );
	for ( auto &&pRender : m_renderList ) pRender->Initialize( d3d9_device() );
	for ( auto &&pTexture : m_textureList ) pTexture->Initialize( d3d9_device() );
#	endif

#	if __has_include( <SRSignal/SRSignal.hpp>)
	onPostReset();
#	endif
}
#endif
